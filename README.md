# GCloud VM Ubuntu


### gcloud auth

#### gcloud auth login

To obtain access credentials using workload identity federation, run:

```bash
gcloud auth login --cred-file=/path/to/workload/configuration/file
```

To list all credentialed accounts and identify the current active account, run:

```bash
gcloud auth list
```

To revoke credentials for a user account (like logging out), run:

```bash
gcloud auth revoke test@gmail.com
```





